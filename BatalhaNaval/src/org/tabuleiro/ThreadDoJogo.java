package org.tabuleiro;

//import java.awt.*;
import java.io.IOException;

import javax.swing.JFrame;

public class ThreadDoJogo extends Thread{

	private Tabuleiro tabuleiro;
	
	public ThreadDoJogo(Tabuleiro tabuleiro) {
		this.tabuleiro = tabuleiro;
	}
	
	@Override
	public void run() {
		while(true) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			tabuleiro.paint(tabuleiro.getGraphics());
		}
	}

//		System.out.println(Tabuleiro.matrix);
//	try {
//		Tabuleiro.Leitura();
//	} catch (IOException e) {
//		e.printStackTrace();
//	}
//	JFrame principal = new JFrame("Batalha Naval");
//	principal.setLocationRelativeTo(null);
//	principal.setMinimumSize(new Dimension(Tabuleiro.largura, Tabuleiro.altura));
//	principal.setMaximumSize(new Dimension(15, 15));
//	principal.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//	principal.setVisible(true);

	
//	}
}
	

