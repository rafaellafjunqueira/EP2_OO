package org.tabuleiro;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;

import javax.swing.*;

public class TelaPrincipal extends JFrame{
	
	private Tabuleiro tabuleiro = new Tabuleiro();
	ThreadDoJogo atualizarThread = new ThreadDoJogo(tabuleiro);
	
	public TelaPrincipal() {
		setLocationRelativeTo(null);
		setMaximumSize(new Dimension(750, 750));
		setSize(tabuleiro.tamanho_quadrado * tabuleiro.getLargura(), tabuleiro.tamanho_quadrado * tabuleiro.getAltura());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		getContentPane().setLayout(new BorderLayout());
		setTitle("Batalha Naval");
		getContentPane().add("Center", tabuleiro);

		atualizarThread.start();

		tabuleiro.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
		        int x = e.getX();
		        int y = e.getY();

		        int x_pos = x/tabuleiro.tamanho_quadrado; 
		        int y_pos = y/tabuleiro.tamanho_quadrado;

		        tabuleiro.atira(x_pos, y_pos);
				
			}

			@Override
			public void mouseClicked(MouseEvent e) {
			}

			@Override
			public void mousePressed(MouseEvent e) {
			}

			@Override
			public void mouseEntered(MouseEvent e) {
			}

			@Override
			public void mouseExited(MouseEvent e) {
			}


		});
	}
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable(){
			public void run() {
				try {
					Tabuleiro.Leitura();
				} catch (IOException e) {
					e.printStackTrace();
				}
				try {
					TelaPrincipal principal = new TelaPrincipal();
					principal.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}