package org.tabuleiro;

import java.awt.Canvas;
import java.awt.*;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

public class Tabuleiro extends Canvas{
	public static int largura;
	public static int altura; 
	public static String [][] matrix;

	public final int tamanho_quadrado = 50;
	
	private int [][] explosao = new int[altura][largura];

	public static void Leitura() throws IOException {
		Scanner ler = new Scanner(System.in);
		System.out.println("Digite o caminho do arquivo txt: ");
		String arquivo = ler.nextLine();
		ler.close();
		
		Path caminho = Paths.get(arquivo);
		
		InputStream doc = new FileInputStream (arquivo);
//		InputStream doc = new FileInputStream ("/home/rafaella/mapa1.txt");
		InputStreamReader leitor = new InputStreamReader(doc);
		BufferedReader buffer = new BufferedReader(leitor);		
		
//	Pegando largura
		
		String larg, alt;
		String s = buffer.readLine();
		s = buffer.readLine();
		larg = s.substring(0, s.indexOf(' '));
		largura = Integer.parseInt(larg);
//		System.out.println(largura);
			
			
//	Pegando altura
		alt = s.substring((s.indexOf(' ')+1),s.length());
		altura = Integer.parseInt(alt);
//		System.out.println(altura);

//	Pegando o tabuleiro / matriz
		s = buffer.readLine();
		s = buffer.readLine();
	
		matrix = new String [altura][largura];
			for(int i = 0; i < altura; i++) {
				s = buffer.readLine(); 
				String mat = s;
				for(int j = 0; j < largura; j++){
					matrix[i][j] = mat.substring(j, j+1);
//					System.out.print(matrix[i][j]);
				}
			}
			
	buffer.close();
	}
	
	public void atira(int x, int y) {
		explosao[x][y] = 1;
	}
	
	public void paint(Graphics g) {
		ImageIcon fundo = new ImageIcon("imagens/aguinha.gif");
		ImageIcon tiro = new ImageIcon("imagens/explosao.gif");
		
		final Image img = fundo.getImage();
		final Image imgShot = tiro.getImage();
		
			for(int i = 0; i < largura; i++) {
				for(int j = 0; j < altura; j++) {	
					g.drawImage(img, i*tamanho_quadrado, j*tamanho_quadrado, tamanho_quadrado, tamanho_quadrado, null);
					if(explosao[i][j] == 1) {
						g.drawImage(imgShot, i*tamanho_quadrado, j*tamanho_quadrado, tamanho_quadrado, tamanho_quadrado, null);
					}
				}
	
			
		}
	}
	
	public int getLargura() {
		return largura;
	}
	public int getAltura() {
		return altura;
	}
	public String[][] getMatrix() {
		return matrix;
	}
}

